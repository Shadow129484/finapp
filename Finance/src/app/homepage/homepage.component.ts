import { Component, OnInit } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {WithdrawBottomsheetComponent} from "../withdraw-bottomsheet/withdraw-bottomsheet.component";
import {DepositBottomsheetComponent} from "../deposit-bottomsheet/deposit-bottomsheet.component";
import {SendBottomsheetComponent} from "../send-bottomsheet/send-bottomsheet.component";
import {ExchangeBottomsheetComponent} from "../exchange-bottomsheet/exchange-bottomsheet.component";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private _bottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {
  }


  openBottomSheet(): void {
    this._bottomSheet.open(WithdrawBottomsheetComponent);
  }

  openDepositBottomSheet(): void {
    this._bottomSheet.open(DepositBottomsheetComponent);
  }
  openSendBottomSheet(): void {
    this._bottomSheet.open(SendBottomsheetComponent);
  }
  openExchangeBottomSheet(): void {
    this._bottomSheet.open(ExchangeBottomsheetComponent);
  }
}
