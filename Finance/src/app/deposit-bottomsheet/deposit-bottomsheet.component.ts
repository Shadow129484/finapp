import { Component, OnInit } from '@angular/core';
import {MatBottomSheetRef} from "@angular/material/bottom-sheet";

@Component({
  selector: 'app-deposit-bottomsheet',
  templateUrl: './deposit-bottomsheet.component.html',
  styleUrls: ['./deposit-bottomsheet.component.css']
})
export class DepositBottomsheetComponent implements OnInit {

  constructor(private _bottomSheetRef: MatBottomSheetRef<DepositBottomsheetComponent>) { }

  ngOnInit(): void {
  }

}
