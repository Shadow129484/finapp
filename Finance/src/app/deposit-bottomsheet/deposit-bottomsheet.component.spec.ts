import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositBottomsheetComponent } from './deposit-bottomsheet.component';

describe('DepositBottomsheetComponent', () => {
  let component: DepositBottomsheetComponent;
  let fixture: ComponentFixture<DepositBottomsheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositBottomsheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositBottomsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
