import { Component, OnInit } from '@angular/core';
import {MatBottomSheetRef} from "@angular/material/bottom-sheet";

@Component({
  selector: 'app-exchange-bottomsheet',
  templateUrl: './exchange-bottomsheet.component.html',
  styleUrls: ['./exchange-bottomsheet.component.css']
})
export class ExchangeBottomsheetComponent implements OnInit {

  constructor(private _bottomSheetRef: MatBottomSheetRef<ExchangeBottomsheetComponent>) { }

  ngOnInit(): void {
  }

}
