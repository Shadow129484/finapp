import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomepageComponent } from './homepage/homepage.component';
import { TransactionComponent } from './transaction/transaction.component';
import { CardsComponent } from './cards/cards.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { TransactionDetailsComponent } from './transaction-details/transaction-details.component';
import { QRcodeComponent } from './qrcode/qrcode.component';
import {SettingsComponent} from "./settings/settings.component";

const routes: Routes = [
  {
    path: '',
    component: QRcodeComponent,

  },
  {
  path: 'login',
  component: LoginComponent,

},
  {
    path: 'homepage',
    component: HomepageComponent,

  },
  {
    path: 'transaction',
    component: TransactionComponent,

  },
  {
    path: 'cards',
    component: CardsComponent,

  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,

  },
  {
    path: 'register',
    component: RegisterComponent,

  },
  {
    path: 'transaction_details',
    component: TransactionDetailsComponent,

  },
  {
    path: 'qrcode',
    component: QRcodeComponent,
  },
  {
    path: 'settings',
    component: SettingsComponent,
  }

];

@NgModule({
  imports: [
        RouterModule.forRoot(routes, {
      useHash: true,
      scrollPositionRestoration: 'top',
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
