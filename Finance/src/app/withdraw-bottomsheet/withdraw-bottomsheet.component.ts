import { Component, OnInit } from '@angular/core';
import {MatBottomSheetRef} from "@angular/material/bottom-sheet";

@Component({
  selector: 'app-withdraw-bottomsheet',
  templateUrl: './withdraw-bottomsheet.component.html',
  styleUrls: ['./withdraw-bottomsheet.component.css']
})
export class WithdrawBottomsheetComponent implements OnInit {

  constructor(private _bottomSheetRef: MatBottomSheetRef<WithdrawBottomsheetComponent>) { }

  ngOnInit(): void {
  }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

}
