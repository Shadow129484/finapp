import {Component, OnInit} from '@angular/core';
import {NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'FinApp';
  showFiller = false;
  loginPage = true;

  constructor(private route: Router) {
  }



  ngOnInit(): void {
    console.log(this.route, 'route');
    this.route.events.subscribe(data => {
      if(data instanceof NavigationStart) {
        console.log(data)
        if (data.url === "/" || data.url === "/login" || data.url === "/register" || data.url === "/forgot-password") {
          this.loginPage = false;
        } else {
          this.loginPage = true;
        }
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });

  }


  check(): void {
    console.log('check')
  }

}
