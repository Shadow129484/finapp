import { Component, OnInit } from '@angular/core';
import {MatBottomSheetRef} from "@angular/material/bottom-sheet";

@Component({
  selector: 'app-send-bottomsheet',
  templateUrl: './send-bottomsheet.component.html',
  styleUrls: ['./send-bottomsheet.component.css']
})
export class SendBottomsheetComponent implements OnInit {

  constructor(private _bottomSheetRef: MatBottomSheetRef<SendBottomsheetComponent>) { }

  ngOnInit(): void {
  }

}
