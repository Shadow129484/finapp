import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendBottomsheetComponent } from './send-bottomsheet.component';

describe('SendBottomsheetComponent', () => {
  let component: SendBottomsheetComponent;
  let fixture: ComponentFixture<SendBottomsheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendBottomsheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendBottomsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
