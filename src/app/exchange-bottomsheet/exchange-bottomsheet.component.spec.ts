import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeBottomsheetComponent } from './exchange-bottomsheet.component';

describe('ExchangeBottomsheetComponent', () => {
  let component: ExchangeBottomsheetComponent;
  let fixture: ComponentFixture<ExchangeBottomsheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExchangeBottomsheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeBottomsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
