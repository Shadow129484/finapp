import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CardsComponent } from './cards/cards.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { TransactionComponent } from './transaction/transaction.component';
import { TransactionDetailsComponent } from './transaction-details/transaction-details.component';
import { IonicModule } from '@ionic/angular';
import { HomepageComponent } from './homepage/homepage.component';
import { QRcodeComponent } from './qrcode/qrcode.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WithdrawBottomsheetComponent } from './withdraw-bottomsheet/withdraw-bottomsheet.component';
import {MatListModule} from "@angular/material/list";
import {MatLineModule} from "@angular/material/core";
import {MatBottomSheet, MatBottomSheetModule} from "@angular/material/bottom-sheet";
import { OverlayModule } from '@angular/cdk/overlay';
import { DepositBottomsheetComponent } from './deposit-bottomsheet/deposit-bottomsheet.component';
import { SendBottomsheetComponent } from './send-bottomsheet/send-bottomsheet.component';
import { ExchangeBottomsheetComponent } from './exchange-bottomsheet/exchange-bottomsheet.component';
import { SettingsComponent } from './settings/settings.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CardsComponent,
    ForgotPasswordComponent,
    RegisterComponent,
    TransactionComponent,
    TransactionDetailsComponent,
    HomepageComponent,
    QRcodeComponent,
    WithdrawBottomsheetComponent,
    DepositBottomsheetComponent,
    SendBottomsheetComponent,
    ExchangeBottomsheetComponent,
    SettingsComponent,

  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        IonicModule.forRoot(),
        BrowserAnimationsModule,
        MatListModule,
        MatLineModule,
        OverlayModule,
        MatSidenavModule,
        MatFormFieldModule,
        MatButtonModule,
        FormsModule
    ],
  entryComponents: [WithdrawBottomsheetComponent],
  providers: [MatBottomSheet ] ,
  bootstrap: [AppComponent]
})
export class AppModule { }
