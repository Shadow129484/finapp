import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawBottomsheetComponent } from './withdraw-bottomsheet.component';

describe('WithdrawBottomsheetComponent', () => {
  let component: WithdrawBottomsheetComponent;
  let fixture: ComponentFixture<WithdrawBottomsheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WithdrawBottomsheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawBottomsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
